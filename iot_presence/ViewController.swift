//
//  ViewController.swift
//  iot_presence
//
//  Created by Fabio Nisci on 26/05/17.
//  Copyright © 2017 Fabio Nisci. All rights reserved.
//

import UIKit
import CocoaMQTT

protocol FNTelemetryPresenceDelegate {
    func telemetryPresence(_ telemetry: FNTelemetryPresence, didConnect host: String, port: Int)
    func telemetryPresence(_ telemetry: FNTelemetryPresence, didReceiveMessage message: CocoaMQTTMessage, id: UInt16 )
    func telemetryPresence(_ telemetry: FNTelemetryPresence, didDisconnectWithError err: Error?)
}

class FNTelemetryPresence: NSObject {

    fileprivate var organizationName:String?
    fileprivate var appIdentifier = Bundle.main.bundleIdentifier!
    
    public private(set) var baseTopic:String?
    public private(set) var client:CocoaMQTT?
    
    var delegate:FNTelemetryPresenceDelegate?
    
    var uuid = UIDevice.current.identifierForVendor!.uuidString
    
    init(username:String? = nil, password:String? = nil, identifier:String? = nil, organizationName:String, host: String = "iot.eclipse.org", port: UInt16 = 1883) {
        super.init()
        
        let clientID = String(ProcessInfo().processIdentifier)
        client = CocoaMQTT(clientID: clientID, host: host, port: port)
        if let client = client {
            client.username = username
            client.password = password
            client.keepAlive = 15
            client.delegate = self
            self.baseTopic = getBaseTopicWith(organization: organizationName, username: username)
            //print(self.baseTopic)
            if let onlineTopic = baseTopic?.appending("online"){
                client.willMessage = CocoaMQTTWill(topic: onlineTopic, message: "0")
            }
        }
    }
    
    func connect(){
        client?.connect()
    }
    
    func listen(_ topic: String, qos: CocoaMQTTQOS = .qos1){
        guard let customTopic = baseTopic?.appending(topic) else{ return}
        client?.subscribe(customTopic, qos: qos)
    }
}

extension FNTelemetryPresence: CocoaMQTTDelegate{
    fileprivate func getBaseTopicWith(organization:String, username:String?)->String?{
        // username missing on public channels
        // c/organization/username/appid/clients/
        
        guard var topic = URL(string: "c/") else { return nil}
        topic = topic.appendingPathComponent(organization)
        if let user = username{
            topic = topic.appendingPathComponent(user)
        }
        topic = topic.appendingPathComponent(appIdentifier).appendingPathComponent("clients").appendingPathComponent(uuid).appendingPathComponent("")
        return topic.absoluteString
    }
    
    
    func mqtt(_ mqtt: CocoaMQTT, didConnect host: String, port: Int){
        guard let versionTopic = baseTopic?.appending("version") else{ return}
        guard let onlineTopic = baseTopic?.appending("online") else{ return }
        mqtt.publish(onlineTopic, withString: "1")
        let version:String? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
        if let version = version{
            let versionMessage = CocoaMQTTMessage(topic: versionTopic, string: version)
            mqtt.publish(versionMessage)
        }
        delegate?.telemetryPresence(self, didConnect: host, port: port)
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16 ){
        delegate?.telemetryPresence(self, didReceiveMessage: message, id: id)
    }
    
    func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?){
        delegate?.telemetryPresence(self, didDisconnectWithError: err)
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopic topic: String){}
    func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopic topic: String){}
    func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck){}
    func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16){}
    func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16){}
    func mqttDidPing(_ mqtt: CocoaMQTT){}
    func mqttDidReceivePong(_ mqtt: CocoaMQTT){}
}

class ViewController: UIViewController {

    let client = FNTelemetryPresence(organizationName: "fabiosoft")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(client.baseTopic!)
        client.delegate = self
        client.connect()
    }


}

extension ViewController: FNTelemetryPresenceDelegate{
    func telemetryPresence(_ telemetry: FNTelemetryPresence, didConnect host: String, port: Int) {
        telemetry.listen("message")
    }
    func telemetryPresence(_ telemetry: FNTelemetryPresence, didDisconnectWithError err: Error?) {
        print(#function)
    }
    func telemetryPresence(_ telemetry: FNTelemetryPresence, didReceiveMessage message: CocoaMQTTMessage, id: UInt16) {
        
    }
}

